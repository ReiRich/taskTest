Установка Maven
Зайдите на официальный сайт мавен в раздел загрузка и скачайте последнюю стабильную версию.
Распакуйте архив в инсталляционную директорию. Например в C:\Program Files\maven\ в Windows или /opt/maven в Linux
Установите переменную окружения M2_HOME: 
В Windows кликните правой кнопкой мыши на "мой компьютер" ->свойства->дополнительные параметры->переменные среды->системные переменные и там добавьте "M2_HOME" и "C:\Program Files\maven\" . 
В Linux можно добавить строку "export M2_HOME=/opt/maven"в файл /etc/profile . 
Установите переменную окружения PATH В Windows в переменной PATH добавьте к списку директорий строку %M2_HOME%\bin". В Linux можно добавить строку "export PATH=$PATH:$M2_HOME/bin"в файл /etc/profile .
Проверьте корректность установки, набрав в командной строке 
mvn -version

Если результат примерно такой
    dima@myhost ~ $ mvn -version
    Apache Maven 3.0 (r1004208; 2010-10-04 15:50:56+0400)
    Java version: 1.6.0_22
    Java home: /opt/sun-jdk-1.6.0.22/jre
    Default locale: ru_RU, platform encoding: UTF-8
    OS name: "linux" version: "2.6.34-gentoo-r12" arch: "amd64" Family: "unix"
                    
то поздравляю, вы успешно установили Maven.
*Во многих дистрибутивах Linux, maven устанавливается автоматически, с помощью менеджера пакетов.

Если что-то не работает
Проверьте, установлен ли у вас JDK.
Для этого наберите в консоли «java -version» ответ должен быть примерно таким:
    java version "1.6.0_22"
    OpenJDK Runtime Environment (IcedTea6 1.10.5) (ArchLinux-6.b22_1.10.5-1-x86_64)
    OpenJDK 64-Bit Server VM (build 19.0-b09, mixed mode)
                
Проверьте установлена ли переменная окружения JAVA_HOME
Если у вас Windows наберите в консоли:
echo %JAVA_HOME%

Если у вас Linux наберите в консоли:
echo $JAVA_HOME

команда должна вывести путь к JDK.

Запуск тестов командой (из директории с файлом pom.xml):
mvn clean test