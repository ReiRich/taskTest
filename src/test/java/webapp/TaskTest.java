package webapp;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Helper.PageObjects;
import Helper.WaitElements;

public class TaskTest {	

	private static WebDriver driver;
		
		@BeforeClass
		public static void setUp() throws Exception {
			System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();	
		}
		
		@Test
		public void headerGuest() throws Exception {
			driver.get("https://www.google.ru/");
			WaitElements.mainPage(driver);
			PageObjects.searchInput(driver).clear();
			PageObjects.searchInput(driver).sendKeys("habrahabr", Keys.ENTER);
			List<WebElement> links = driver.findElements(By.xpath("//cite[contains(text(), 'habrahabr.ru')]"));
			String link = links.get(0).getText();
			driver.get(link);
			WaitElements.habrPage(driver);
			PageObjects.sandbox(driver).click();
			WaitElements.sandboxInside(driver);
			List<WebElement> pagination = driver.findElements(By.cssSelector("a.toggle-menu__item-link.toggle-menu__item-link_pagination"));
			for (int i = 0; i < pagination.size(); i++) {
			    String title = pagination.get(i).getAttribute("href");
			    if (title.contains("page2")) {
			        pagination.get(i).click();
			        break;
			    }			   
			}
		}
		
		@AfterClass
		public static void end() throws Exception {
			driver.quit();
		}
	}