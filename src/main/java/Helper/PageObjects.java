package Helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageObjects {

	public static WebElement searchInput(WebDriver driver) {
		return driver.findElement(By.xpath("//input[@id='lst-ib']"));
	}
	
	public static WebElement sandbox(WebDriver driver) {
		return driver.findElement(By.xpath("//a[contains(text(),'Песочница')]"));
	}
}